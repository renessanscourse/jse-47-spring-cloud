package ru.ovechkin.tm.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.config.WebMvcConfig;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumerated.Role;
import ru.ovechkin.tm.exeption.unknown.ProjectUnknownException;
import ru.ovechkin.tm.repository.UserRepository;
import ru.ovechkin.tm.rest.controller.AuthenticationRestController;
import ru.ovechkin.tm.util.UserUtil;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebMvcConfig.class)
public class ProjectServiceTest {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private AuthenticationRestController authenticationRestController;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private final Project project = new Project();

    {
        project.setName("test");
        project.setDescription("test");
    }

    @Before
    public void setUp() throws Exception {
        final String login = "test_user";
        final String password = "test_user";
        final User user = new User();
        user.setId("constantId");
        user.setLogin(login);
        user.setPasswordHash(passwordEncoder.encode(password));
        user.setRole(Role.USER);
        userRepository.save(user);
        authenticationRestController.login(login, password);
    }

    @Test
    public void testFindAllUserProjects() {
        projectService.save(new Project(), UserUtil.getUser());
        projectService.save(new Project(), UserUtil.getUser());
        Assert.assertEquals(2, projectService.findAllUserProjects(UserUtil.getUser()).size());
    }

    @Test
    public void testSave() {
        projectService.save(project, UserUtil.getUser());
        Assert.assertEquals(1, projectService.findAllUserProjects(UserUtil.getUser()).size());
    }

    @Test
    public void testRemoveById() {
        projectService.save(project, UserUtil.getUser());
        Assert.assertEquals(
                project.toString(),
                projectService.findById(project.getId(), UserUtil.getUser()).toString());
        projectService.removeById(project.getId(), UserUtil.getUser());
        Assert.assertThrows(
                ProjectUnknownException.class,
                () -> projectService.findById(project.getId(), UserUtil.getUser()));
    }

    @Test
    public void testFindById() {
        projectService.save(project, UserUtil.getUser());
        Assert.assertEquals(
                project.toString(),
                projectService.findById(project.getId(), UserUtil.getUser()).toString());
    }

    @Test
    public void testUpdateById() {
        projectService.save(project, UserUtil.getUser());
        Assert.assertEquals(
                project.toString(),
                projectService.findById(project.getId(), UserUtil.getUser()).toString());
        final Project projectUpdated = new Project();
        projectUpdated.setName("UPDATED");
        projectUpdated.setDescription("UPDATED");
        projectService.updateById(project.getId(), projectUpdated, UserUtil.getUser());
    }

}