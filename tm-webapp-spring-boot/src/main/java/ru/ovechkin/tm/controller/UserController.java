package ru.ovechkin.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.ovechkin.tm.api.service.IUserService;
import ru.ovechkin.tm.entity.User;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService userService;

    @GetMapping("/registryForm")
    public String getRegistryForm(Model model) {
        model.addAttribute("user", new User());
        return "registry-form";
    }

    @PostMapping("/register")
    public String register(@ModelAttribute User user) {
        userService.registry(user);
        return "redirect:/";
    }

}