package ru.ovechkin.tm.exeption.other;

public class WrongCurrentPasswordException extends RuntimeException {

    public WrongCurrentPasswordException() {
        super("Error! wrong current password...");
    }

}