package ru.ovechkin.tm.controller.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.ovechkin.tm.client.IProjectRestController;
import ru.ovechkin.tm.client.ITaskRestController;
import ru.ovechkin.tm.entity.Task;

import java.util.List;

@RestController
@RequestMapping("/api/rest/tasks")
public class TaskRestController {

    @Autowired
    private ITaskRestController taskRestController;

    @Autowired
    private IProjectRestController projectRestController;

    @GetMapping(value = "/{projectId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Task> show(@PathVariable("projectId") final String projectId) {
        return taskRestController.show(projectId);
    }

    @PutMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Task> create(
            @RequestParam("projectId") String projectId,
            @RequestBody Task task
    ) {
        return taskRestController.create(projectId, task);
    }

    @DeleteMapping(value = "/remove", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Task> remove(
            @RequestParam("taskId") String taskId,
            @RequestParam("projectId") String projectId
    ) {
        return taskRestController.remove(taskId, projectId);
    }

    @PostMapping(value = "/edit", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Task> edit(
            @RequestBody final Task task,
            @RequestParam("projectId") final String projectId,
            @RequestParam("taskId") final String taskId
    ) {
        return taskRestController.edit(task, projectId, taskId);
    }

}